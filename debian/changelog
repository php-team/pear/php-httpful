php-httpful (1.0.0-2) unstable; urgency=medium

  * Drop coverage (Closes: #1070555)

 -- David Prévot <taffit@debian.org>  Sun, 19 May 2024 23:48:06 +0200

php-httpful (1.0.0-1) unstable; urgency=medium

  [ Nate Good ]
  * Release 1.0.0 security fixes, php v8.1+

  [ David Prévot ]
  * Update Standards-Version to 4.7.0

 -- David Prévot <taffit@debian.org>  Thu, 02 May 2024 19:38:50 +0200

php-httpful (0.3.2-2) unstable; urgency=medium

  * Fix d/watch to match updated GitHub URL
  * Use DEP-14 branch name
  * Install /u/s/pkg-php-tools/{autoloaders,overrides} files
  * Use secure URI in Homepage field.
  * Bump debhelper from old 12 to 13.
  * Set Rules-Requires-Root: no.
  * Use dh-sequence-phpcomposer instead of --with phpcomposer
  * Drop now useless nocheck condition
  * Update Standards-Version to 4.6.1

 -- David Prévot <taffit@debian.org>  Sun, 26 Jun 2022 08:40:12 +0200

php-httpful (0.3.2-1) unstable; urgency=medium

  [ Andrzej Dybowski ]
  * add JsonParseException

  [ atymic ]
  * fix: allow case insensitive header access

  [ David Prévot ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update Standards-Version to 4.5.0

 -- David Prévot <taffit@debian.org>  Sun, 02 Feb 2020 00:09:08 -1000

php-httpful (0.3.0-1) unstable; urgency=medium

  [ Nate Good ]
  * PHPUnit Fixes and officially drop < 7.2

 -- David Prévot <taffit@debian.org>  Thu, 02 Jan 2020 08:43:12 +1100

php-httpful (0.2.20-2) unstable; urgency=medium

  * Set upstream metadata fields: Repository, Repository-Browse.

 -- David Prévot <taffit@debian.org>  Mon, 09 Dec 2019 13:05:30 -1000

php-httpful (0.2.20-1) unstable; urgency=medium

  * Initial release (new php-amqplib build-dependency)

 -- David Prévot <taffit@debian.org>  Thu, 21 Nov 2019 07:33:50 -1000
